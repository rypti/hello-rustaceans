# Hello-Rustaceans

# Rules :
## Pipeline :
- Update version only on merge request (main|dev)
  - Major
  - Minor
  - Patch
- Create release only on version change (main|dev)
- Upload binary only on version change (main|dev)
- Launch 
  - Cargo test only on merge request
    - Don't allow merge on fail
  - Cargo build only on merge request 
    - Don't allow merge on fail
- Differentiate "main" and "dev" binary/release

# Pipeline stages:
- get_commit_infos
- verify_image
- test_code (cargo test)
- build_binary
- upload
- release